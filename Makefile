# For mirroring from website
MIRROR_DOWNLOAD=0
EXTERNAL_MEDIA=/media/LACIE/fedora/f12-local-repo

# Set SOURCES
ifeq ($(MIRROR_DOWNLOAD),1)
  MIRROR_SRC=rsync://mirror.ovh.net/download.fedora.redhat.com/linux
  RPMFUSION_SRC=rsync://download1.rpmfusion.org/$(RPMFUSION)
else
  MIRROR_SRC=$(EXTERNAL_MEDIA)/fedora
  RPMFUSION_SRC=$(EXTERNAL_MEDIA)/rpmfusion
endif

# Set TARGET directory
ifeq ($(MIRROR_DOWNLOAD),1)
  # Location of downloaded mirror
  TARGET_DIR=/tmp/f12-local-repo
else
  # Location of Fedora offline repository
  TARGET_DIR=/var/ftp/pub
endif

# Definitions
DISTRO=fedora
RELEASEVER=12
BASEARCH=i386
RPMFUSION=rpmfusion

#
# You shouldn't have to change anything below this, 
# unless you really have insurance.
#

# Rsync
RSYNC=rsync
RSYNC_FLAGS=-H -auvr --progress

# Directory paths for releases, updates 
UPDATES_PATH=updates/$(RELEASEVER)/$(BASEARCH)
RELEASES_PATH=releases/$(RELEASEVER)/Everything/$(BASEARCH)/os

# Directory paths for rpmfusion repositories
DIR_RPMFUSION_FREE_RELEASES=free/$(DISTRO)/$(RELEASES_PATH)
DIR_RPMFUSION_FREE_UPDATES=free/$(DISTRO)/$(UPDATES_PATH)
DIR_RPMFUSION_NONFREE_RELEASES=nonfree/$(DISTRO)/$(RELEASES_PATH)
DIR_RPMFUSION_NONFREE_UPDATES=nonfree/$(DISTRO)/$(UPDATES_PATH)

MKDIR_OPTIONS=-m 755 -p

all: mirror_download mirror_copy

mirror_download: welcome_msg check_rsync make_dirs rsync_repo

mirror_copy: welcome_msg check_vsftpd check_rsync make_dirs rsync_repo

# Create required repository directories under TARGET_DIR
make_dirs: mkdir_releases mkdir_updates mkdir_rpmfusion

mkdir_releases: check_vsftpd
	mkdir $(MKDIR_OPTIONS) $(TARGET_DIR)/$(DISTRO)/$(RELEASES_PATH)

mkdir_updates: check_vsftpd
	mkdir $(MKDIR_OPTIONS) $(TARGET_DIR)/$(DISTRO)/$(UPDATES_PATH)

mkdir_rpmfusion: mkdir_rpmfusion_free_releases \
			mkdir_rpmfusion_free_updates \
			mkdir_rpmfusion_nonfree_releases \
			mkdir_rpmfusion_nonfree_updates

mkdir_rpmfusion_free_releases: check_vsftpd
	mkdir $(MKDIR_OPTIONS) $(TARGET_DIR)/$(RPMFUSION)/$(DIR_RPMFUSION_FREE_RELEASES)

mkdir_rpmfusion_free_updates: check_vsftpd
	mkdir $(MKDIR_OPTIONS) $(TARGET_DIR)/$(RPMFUSION)/$(DIR_RPMFUSION_FREE_UPDATES)

mkdir_rpmfusion_nonfree_releases: check_vsftpd
	mkdir $(MKDIR_OPTIONS) $(TARGET_DIR)/$(RPMFUSION)/$(DIR_RPMFUSION_NONFREE_RELEASES)

mkdir_rpmfusion_nonfree_updates: check_vsftpd
	mkdir $(MKDIR_OPTIONS) $(TARGET_DIR)/$(RPMFUSION)/$(DIR_RPMFUSION_NONFREE_UPDATES)

# Rsync offline repository with updated repository on external media
rsync_repo: rsync_releases \
		rsync_updates \
		rsync_rpmfusion_free_releases \
		rsync_rpmfusion_free_updates \
		rsync_rpmfusion_nonfree_releases \
		rsync_rpmfusion_nonfree_updates

rsync_releases: check_rsync 
	$(RSYNC) $(RSYNC_FLAGS) $(MIRROR_SRC)/$(RELEASES_PATH)/ $(TARGET_DIR)/$(DISTRO)/$(RELEASES_PATH)/

rsync_updates: check_rsync
	$(RSYNC) $(RSYNC_FLAGS) $(MIRROR_SRC)/$(UPDATES_PATH)/ $(TARGET_DIR)/$(DISTRO)/$(UPDATES_PATH)/

rsync_rpmfusion_free_releases: check_rsync
	$(RSYNC) $(RSYNC_FLAGS) $(RPMFUSION_SRC)/$(DIR_RPMFUSION_FREE_RELEASES)/ \
		$(TARGET_DIR)/$(RPMFUSION)/$(DIR_RPMFUSION_FREE_RELEASES)/

rsync_rpmfusion_free_updates: check_rsync
	$(RSYNC) $(RSYNC_FLAGS) $(RPMFUSION_SRC)/$(DIR_RPMFUSION_FREE_UPDATES)/ \
		$(TARGET_DIR)/$(RPMFUSION)/$(DIR_RPMFUSION_FREE_UPDATES)/

rsync_rpmfusion_nonfree_releases: check_rsync
	$(RSYNC) $(RSYNC_FLAGS) $(RPMFUSION_SRC)/$(DIR_RPMFUSION_NONFREE_RELEASES)/ \
		$(TARGET_DIR)/$(RPMFUSION)/$(DIR_RPMFUSION_NONFREE_RELEASES)/

rsync_rpmfusion_nonfree_updates: check_rsync
	$(RSYNC) $(RSYNC_FLAGS) $(RPMFUSION_SRC)/$(DIR_RPMFUSION_NONFREE_UPDATES)/ \
		$(TARGET_DIR)/$(RPMFUSION)/$(DIR_RPMFUSION_NONFREE_UPDATES)/

welcome_msg:
	@echo "" 
	@echo "Welcome to            " 
	@echo "      __          _       "
	@echo "     / _| ___  __| |_  __ "
	@echo "    | |_ / _ \/ _\` \ \/ /"
	@echo "    |  _|  __/ (_| |>  <  "
	@echo "    |_|  \___|\__,_/_/\_\ repo setup"
	@echo ""

check_vsftpd: welcome_msg
	@echo -n "Checking for vsftpd ..."; \
	value=`rpm -q vsftpd | grep -v "not installed" | wc -l`; \
	if [ $$value -eq 0 ]; then \
		echo "vsftpd is not installed."; \
		echo "Installing vsftpd ... "; \
		if [ -f $(SOURCE)/$(DISTRO)/$(UPDATES_PATH) ]; then \
			rpm -ivh $(SOURCE)/$(DISTRO)/$(UPDATES_PATH)/vsftpd*; \
		else \
			yum install vsftpd*; \
		fi \
	else \
		echo -e " installed.\n"; \
	fi

check_rsync: welcome_msg check_vsftpd make_dirs
	@echo -ne "\nChecking for rsync ..."; \
	value=`rpm -q rsync | grep -v "not installed" | wc -l`; \
	if [ $$value -eq 0 ]; then \
		echo "rsync is not installed."; \
		echo "Installing rsync ..."; \
		if [ -f $(SOURCE)/$(DISTRO)/$(UPDATES_PATH) ]; then \
			rpm -ivh $(SOURCE)/$(DISTRO)/$(UPDATES_PATH)/rsync*; \
		else \
			yum install rsync; \
		fi \
	else \
		echo -e " installed.\n"; \
	fi
